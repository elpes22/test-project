import { Component, OnInit } from '@angular/core';
import { Product, Products} from '../../models/product.model'
import { Http } from './http'
import { log } from 'util';
@Component({
  selector: 'app-goods',
  templateUrl: './goods.component.html',
  styleUrls: ['./goods.component.css']
})
export class GoodsComponent implements OnInit {

  constructor() { 
    
  }
  public laptops;
  ngOnInit() {
    this.laptops.get('laptops.json').subscribe(
      value => {
        return value;

      },
      error => {
          console.log(error);
          
      }
    )
    console.log(this.laptops);

  }
  public products: Product[] = [
    // new Product('NOKIA', 'NOKIA N1','SMARTPHONE','UAH', false, 2000)
      new Product('NOKIA', 'NOKIA n2', false),
      new Product('NOKIA', 'NOKIA n1', false)
    
  ]
     


}
