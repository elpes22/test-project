import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { Action } from '@ngrx/store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GoodsComponent } from './components/goods/goods.component';
import { ProductComponent } from './components/product/product.component';
import { FilterTopComponent } from './components/filter-top/filter-top.component';
import { FilterLeftComponent } from './components/filter-left/filter-left.component';

@NgModule({
  declarations: [
    AppComponent,
    GoodsComponent,
    ProductComponent,
    FilterTopComponent,
    FilterLeftComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
