import { Action } from '@ngrx/store';
import * as productAction from '../actions/products';
import { Product, Products } from '../../models/product.model';